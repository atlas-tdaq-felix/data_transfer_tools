
import random
import math
import struct

BLOCK_HEADER_SIZE = 4
BLOCK_SEQNR_MASK = 0x1F


def random_chunks(min_size=128, max_size=4096, elink=0, streams=[]):  # noqa: B006

    """Generator which yields randomly sized chunks as bytes."""
    # status = 0
    # gbt_and_elink_id = elink
    type_orbit_bcid = 0
    l1id = 0

    streamIndex = 0
    symbol = 0
    while True:
        size = random.randint(min_size, max_size)
        chunk = bytearray(bytes(chr(symbol) * size, encoding='utf8'))

        offset = 0
        if len(streams) > 0:
            stream = streams[streamIndex]
            streamIndex += 1
            streamIndex %= len(streams)
            struct.pack_into("B", chunk, offset, stream)
            offset += 1

        struct.pack_into("HH", chunk, offset, type_orbit_bcid, l1id)
        struct.pack_into("BBBB", chunk, size - 4, 255, 255, 255, 255)  # Trailer is all 0xFF

        yield bytes(chunk)

        type_orbit_bcid = (type_orbit_bcid + 1) % 128
        l1id = (l1id + 1) % 128
        symbol = (symbol + 1) % 256


_typenames = [(0, 'null'),
              (1, 'first'),
              (2, 'last'),
              (3, 'both'),
              (4, 'middle'),
              (5, 'timeout'),
              (7, 'outofband')]


def subchunk_trailer(sub_type, length, trailer_size, trunc=False, chunk_error=False, crc_error=False, busy=False):
    """Pack the given subchunk trailer information in a 32 or 16 bit word."""
    trailer = bytearray(trailer_size)
    offset = 2 if trailer_size == 4 else 0
    for i, name in _typenames:
        if name == sub_type:
            trailer[1 + offset] = i << 5
            break
    else:
        raise ValueError("type argument must be one of 'first', 'last', 'both', 'middle', 'null'")

    if trunc:
        trailer[1 + offset] += 1 << 4

    if chunk_error:
        trailer[1 + offset] += 1 << 3

    if crc_error:
        trailer[1 + offset] += 1 << 2

    if busy and (offset == 2):
        trailer[1 + offset] += 1 << 1

    if offset == 0:
        if (length > 1023):
            raise ValueError("Subchunk length must be <= 1023 Byte for 2 byte trailers")
        trailer[1] += (length & 0x0300) >> 8
        trailer[0] = length & 0xFF
    else:
        if (length >= 1024*64):
            raise ValueError("Subchunk length must be < 64 kByte for 4 byte trailers")
        trailer[2] = 0
        trailer[1] = (length & 0xFF00) >> 8
        trailer[0] = length & 0xFF

    return trailer


def decode_subchunk_trailer(trailer):
    """Decode the given subchunk trailer byte sequence and return the result as dict."""
    trailer_size = len(trailer)
    offset = 2 if trailer_size == 4 else 0

    t = dict()
    typeid = trailer[1 + offset] >> 5
    for i, name in _typenames:
        if i == typeid:
            t['type'] = name
            break
    else:
        raise ValueError("Malformed subchunk trailer '{:02X} {:02X}': "
                         "type must be in range 0-4".format(trailer[0 + offset], trailer[1 + offset]))

    t['trunc'] = (trailer[1 + offset] & (1 << 4)) > 0
    t['chunk_error'] = (trailer[1 + offset] & (1 << 3)) > 0
    t['crc_error'] = (trailer[1 + offset] & (1 << 2)) > 0

    if offset == 0:
        t['busy'] = 0
        t['length'] = trailer[0] + ((trailer[1] & 3) << 8)
    else:
        t['busy'] = (trailer[3] & (1 << 1)) > 0
        t['length'] = trailer[0] + (trailer[1] << 8)

    return t


def _subchunktype(subchunk_type, words_left_in_block, words_left_in_chunk):
    """Return the type of a subchunk ('first', 'last', 'middle', or 'both')."""
    trailer_words = 1

    if (subchunk_type == "first") or (subchunk_type == "middle"):
        # subchunk is a continuation from a subchunk in a previous block
        if words_left_in_chunk + trailer_words <= words_left_in_block:
            subchunk_type = 'last'
        else:
            subchunk_type = 'middle'
    else:
        if words_left_in_chunk + trailer_words <= words_left_in_block:
            subchunk_type = 'both'
        else:
            subchunk_type = 'first'

    return subchunk_type


def _add_chunk_to_block(block, blockpos, chunk, subchunk_type, trailer_size):
    """
    Add a chunk to a block.

    If the chunk does not fit into the block, it
    is divided into subchunks. The fitting subchunk is added to the block, the
    other subchunk (rest of the chunk) is returned to the caller.

    Returns a tuple (block, blockpos, chunk, subchunk_type).
    """
    word_size = trailer_size
    empty_subchunk = subchunk_trailer('null', 0, trailer_size)

    words_left_in_block = int((len(block) - blockpos) / word_size)
    if words_left_in_block == 0:
        return block, blockpos, chunk, subchunk_type
    elif words_left_in_block == 1:
        block[blockpos:blockpos + trailer_size] = empty_subchunk
        return block, blockpos + trailer_size, chunk, 'null'
    else:
        subchunklen = min(words_left_in_block * word_size - trailer_size, len(chunk))
        payload = chunk[0:subchunklen]
        subchunk_type = _subchunktype(subchunk_type, words_left_in_block, math.ceil(float(len(chunk)) / word_size))
        # subchunk_type = _subchunktype(subchunk_type, words_left_in_block, (len(chunk) + word_size) / word_size)
        trailer = subchunk_trailer(subchunk_type, subchunklen, trailer_size)

        block[blockpos:blockpos + subchunklen] = payload
        blockpos += subchunklen
        while blockpos % word_size != 0:
            block[blockpos] = 0
            blockpos += 1
        block[blockpos:blockpos + trailer_size] = trailer
        blockpos += trailer_size

        return block, blockpos, chunk[subchunklen:], subchunk_type


def block_header(sob, elink, seqnr, block_size):
    """Generate a 4 Byte block header."""

    if sob == 0xC0CE:
        sob = 0xC0CE + (((block_size >> 10) - 1) * 0x100)

    header = bytearray(4)
    header[0] = elink & 0xFF
    header[1] = ((elink & 0x700) >> 8) | ((seqnr & 0x1F) << 3)
    header[2] = sob & 0xFF
    header[3] = (sob >> 8) & 0xFF

    return header


def _new_block(sob, elink, seqnr, block_size):
    block = bytearray(block_size)
    block[0:4] = block_header(sob, elink, seqnr, block_size)
    return block, 4, (seqnr + 1) & BLOCK_SEQNR_MASK


def encode_blocks(chunk_iterable, sob, elink, block_size, seqnr=0, trailer_size=4):
    """Generator which encodes chunks in block_size blocks with appropriate sob header
    and trailer information."""

    block, blockpos, seqnr = _new_block(sob, elink, seqnr, block_size)
    for chunk in chunk_iterable:
        subchunk_type = "empty"
        while len(chunk) > 0:
            block, blockpos, chunk, subchunk_type = _add_chunk_to_block(block, blockpos, chunk, subchunk_type, trailer_size)
            assert blockpos <= len(block)
            if blockpos == len(block):
                yield block
                block, blockpos, seqnr = _new_block(sob, elink, seqnr, block_size)

    yield block


def decode_block_header(header):
    sob = header[2] | (header[3] << 8)
    return {
        'sob': sob,
        'elink': header[0] | ((header[1] & 0x07) << 8),
        'seqnr': (header[1] & 0xF8) >> 3,
        'block_size': 1024 if sob == 0xABCD else (header[3] - 0xC0 + 1) * 1024,
        'trailer_size': 2 if (sob == 0xABCD) else 4
    }


class Block(object):
    """Objects of this class represent FELIX blocks.
    Objects can be created from byte sequences using the alternative
    from_bytes constructor.
    """

    def __init__(self, elink, seqnr, block_size, trailer_size):
        self.elink = elink
        self.seqnr = seqnr
        self.block_size = block_size
        self.trailer_size = trailer_size
        self.data = bytearray(self.block_size - BLOCK_HEADER_SIZE)

    @classmethod
    def from_bytes(cls, byteseq):
        """Construct a Block object from a byte sequence."""

        block_size = len(byteseq)
        header = decode_block_header(byteseq[0:BLOCK_HEADER_SIZE])
        assert (header['sob'] == 0xABCD) or (header['sob'] & 0x00FF == 0x00CE)
        assert header['block_size'] == block_size
        block = cls(header['elink'], header['seqnr'], header['block_size'], header['trailer_size'])
        block.data = byteseq[BLOCK_HEADER_SIZE:]
        return block

    def subchunks(self, noreverse=False):
        """A generator iterating of the subchunks of a block."""
        word_size = self.trailer_size

        subchunks = []
        pos = len(self.data)
        while pos > self.trailer_size:
            trailer = decode_subchunk_trailer(self.data[pos - self.trailer_size:pos])

            pos -= trailer['length'] + self.trailer_size
            while pos % word_size != 0:
                pos -= 1
            if trailer['type'] != 'null':
                pad = (word_size - trailer['length'] % word_size) % word_size
                subchunks.append(Subchunk.from_bytes(self,
                                                     self.data[pos:pos + trailer['length'] + pad + self.trailer_size]))
                if noreverse:
                    yield subchunks[-1]

        # the order of the subchunks needs to be reversed since we read from back to front
        if not noreverse:
            for sc in reversed(subchunks):
                yield sc


class Subchunk(object):
    """Blocks are composed of subchunks, which are represented by this class."""

    def __init__(self, block, length, sub_type, trunc, chunk_error):
        self.block = block
        self.length = length
        self.type = sub_type
        self.trunc = trunc
        self.chunk_error = chunk_error
        self.data = bytearray(length)

    @classmethod
    def from_bytes(cls, block, byteseq):
        """Alternative constructor to create a new subchunk from a byte sequence."""

        assert len(byteseq) >= block.trailer_size

        trailer = decode_subchunk_trailer(byteseq[-block.trailer_size:])
        subchunk = cls(block, trailer['length'], trailer['type'],
                       trailer['trunc'], trailer['chunk_error'])

        subchunk.data = byteseq[0:trailer['length']]
        return subchunk

    def __len__(self):
        return self.length


class Chunk(object):
    """A chunk is a variable-length FELIX packet. For transmission over the
    PCIe bus it is split up in subchunks and organized in fixed-length blocks.
    """

    def __init__(self):
        self.state = 'empty'   # 'empty', 'middle', 'full', or 'error'
        self.data = bytearray(0)
        self.blocks = set()

    def add_subchunk(self, subchunk):
        """Adds a subchunk to the chunk and updates the state of the chunk."""

        if subchunk.type == 'null':
            return

        self.data += subchunk.data
        self.blocks.add(subchunk.block)

        if self.state == 'empty':
            if subchunk.type == 'first':
                self.state = 'middle'
            elif subchunk.type == 'both':
                self.state = 'full'
            else:
                self.state = 'error'
        elif self.state == 'middle':
            if subchunk.type == 'middle':
                self.state = 'middle'
            elif subchunk.type == 'last':
                self.state = 'full'
            else:
                self.state = 'error'
        else:
            self.state = 'error'

    def __len__(self):
        return len(self.data)


def decode_blocks(block_iterable, yield_error_chunks=False):
    """Decodes a sequence of blocks and returns Chunks.

    block_iterable: An iterable returning fixed-size byte sequences (blocks).
    """

    chunk = Chunk()

    for block_in_bytes in block_iterable:
        block = Block.from_bytes(block_in_bytes)

        for subchunk in block.subchunks():
            chunk.add_subchunk(subchunk)
            if chunk.state == 'full':
                yield chunk
                chunk = Chunk()

            if chunk.state == 'error':
                if yield_error_chunks:
                    yield chunk
                chunk = Chunk()
