#!/usr/bin/env python3

import os
import multiprocessing

from rcpy import Controllable
# import ers

from utils import datagen
from block import encode_blocks, random_chunks


def packet_generator(x):
    min_chunksize = 128
    max_chunksize = 4096
    return encode_blocks(random_chunks(min_chunksize, max_chunksize))


def run(fifos):
    fds = [os.open(f, os.O_WRONLY) for f in fifos]
    datagen(fds, dict((fd, packet_generator(1024)) for fd in fds))


class FelixDataGenerator(Controllable):
    def __init__(self):
        super(FelixDataGenerator, self).__init__()

        self.application_name = os.environ.get('TDAQ_APPLICATION_NAME')
        self.fifos = ['/tmp/gbt_%s' % self.application_name]
        self.proc = None

    def configure(self, TransitionCmd):
        print("-> Environment:", str(os.environ))

        for f in self.fifos:
            if not os.path.exists(f):
                os.mkfifo(f)

    def connect(self, TransitionCmd):
        pass

    def prepareForRun(self, TransitionCmd):
        self.proc = multiprocessing.Process(target=run, args=(self.fifos,), name="FelixDataGen_Worker")
        self.proc.start()
        # run(self.fifos)

    def stopROIB(self, TransitionCmd):
        if self.proc.is_alive():
            self.proc.terminate()
        self.proc = None

    def stopDC(self, TransitionCmd):
        pass

    def stopHLT(self, TransitionCmd):
        pass

    def stopRecording(self, TransitionCmd):
        pass

    def stopGathering(self, TransitionCmd):
        pass

    def stopArchiving(self, TransitionCmd):
        pass

    def disconnect(self, TransitionCmd):
        pass

    def unconfigure(self, TransitionCmd):
        for f in self.fifos:
            os.remove(f)

    def subTransition(self, SubTransitionCmd):
        pass

    def resynch(self, ResynchCmd):
        pass

    def user(self, UserCmd):
        pass

    def onExit(self, FSM_STATE):
        pass

    def publish(self):
        pass

    def publishFullStats(self):
        pass

    def disable(self, listOfStrings):
        pass

    def enable(self, listOfStrings):
        pass
