import selectors
import time
import os


def datagen(fds, packet_generators, num_packets=0, max_time=0, delay=0):
    """Function that writes packets to the given FDs.

    fds: A list of filedescriptors
    num_packets: The number of packets to write, or 0 for infinite
    max_time: The number of seconds to run the packet generation, or 0 for infinite
    delay: The minimum delay between two packets on a file descriptor
    packet_generators: A dict-like object mapping file descriptors to a generator
      function which returns packets for that file descriptor.
    """

    data_written = dict((fd, 0) for fd in fds)

    selector = None
    try:
        selector = selectors.PollSelector()
    except AttributeError:
        selector = selectors.DefaultSelector()
    for fd in fds:
        selector.register(fd, selectors.EVENT_WRITE)

    last_sent_times = dict((fd, time.time()) for fd in fds)
    packets_send = dict((fd, 0) for fd in fds)
    start_time = time.time()
    dict((fd, False) for fd in fds)

    try:
        while True:

            if num_packets > 0 and all([packets_send[fd] >= num_packets for fd in fds]):
                break

            if max_time > 0 and time.time() - start_time > max_time:
                break

            events = selector.select()
            for key, _event in events:
                fd = key.fd
                if delay == 0 or time.time() - last_sent_times[fd] > delay:
                    packet = next(packet_generators[fd])
                    os.write(fd, packet)
                    data_written[fd] += len(packet)
                    packets_send[fd] += 1
                    if delay > 0:
                        last_sent_times[fd] = time.time()

    except (KeyboardInterrupt, OSError):
        pass
    except Exception as e:
        raise e
    finally:
        time_delta = time.time() - start_time
        return data_written, packets_send, time_delta  # noqa: B012
