#!/usr/bin/env python3

from setuptools import setup, find_packages
setup(
    name="felix-data-transfer-tools",
    version="0.7.1",
    packages=find_packages(),
    scripts=['scripts/felix-datagen', 'scripts/felix-block-decode', 'scripts/merge-block-files', 'scripts/felix-encode-block-data'],

    install_requires=['docopt'],

    package_data={
        # If any package contains *.txt, *.md, or *.rst files, include them:
        '': ['*.txt', '*.md', '*.rst'],
    },

    # metadata
    author="Joern Schumacher",
    author_email="joern.schumacher@cern.ch",
    description="Tools for emulating FELIX data sources and transfer analyzers"
    # could also include download_url, classifiers, etc.
)
