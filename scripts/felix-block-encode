#!/usr/bin/env python3

"""felix-block-encode - Generates felix block files

The script will produce random data, encode it, and write it to a file.
The file can be read and re-read by the felix-file2host simulator.

Usage:
  felix-block-encode [options --stream=<num>...] <num> <file>

Options:
  -b=<size> --block-size=<size>   Block size in bytes. [Default: 1024]
  --min-chunksize=<size>          Minimum size of the randomly sized chunks that
                                  are generated in byte. [Default: 128]
  --max-chunksize=<size>          Maximum size of the randomly sized chunks that
                                  are generated in byte. [Default: 4096]
  -u --stream=<num>...            Data are encoded using the given stream IDs: 0..0xff.
  -e --elink=<num>                Blocks are encoded using the given elink
                                  number. [Default: 0]
  -t --trailer-size=<bytes>       Trailer size in bytes [default: 4]
  -s --sob=<start_of_block>       Override block header [default: 0xC0CE]
  -h --help                       Display this help message.
  -v --version                    Display version.

Arguments:
  <num>                           Number of blocks to be generated.
  <file>                          File to be written.
"""
import os
import sys

from docopt import docopt

from felix.block import encode_blocks, random_chunks

version = (4, 1, 1)
version_string = 'felix-block-encode v{0:}.{1:}.{2:}'.format(version[0], version[1], version[2])


def main(args):
    arguments = docopt(__doc__, args[1:], version=version_string)

    min_chunksize = int(arguments['--min-chunksize'], 0)
    max_chunksize = int(arguments['--max-chunksize'], 0)
    elink = int(arguments['--elink'], 0)
    streams = [int(i, 0) for i in arguments['--stream']]
    block_size = int(arguments['--block-size'], 0)
    trailer_size = int(arguments['--trailer-size'], 0)
    suggested_sob = int(arguments['--sob'], 0)

    num_blocks = int(arguments['<num>'], 0)
    out_file = arguments['<file>']

    if block_size % 1024:
        block_size = ((block_size >> 10) + 1) << 10
        print('block size adjusted to next 1024 byte boundary:', block_size)

    if (trailer_size != 2) and (trailer_size != 4):
        print('Supported trailer size is 2 or 4')
        sys.exit(-1)

    if trailer_size == 2:
        block_size = 1024
        sob = 0xABCD if suggested_sob == 0xC0CE else suggested_sob
        print('defaulting to sob 0xABCD for trailer size 2')
    else:
        block_size = max(1024, block_size)
        block_size = min(block_size, 64*1024)
        sob = 0xC0CE if suggested_sob == 0xC0CE else suggested_sob

    if num_blocks % 32:
        num_blocks = ((num_blocks >> 5) + 1) << 5
        print('number of blocks adjusted to next 32 bit boundary for continuous sequence number:', num_blocks)

    generator = encode_blocks(random_chunks(min_chunksize, max_chunksize, elink, streams),
                              sob, elink, seqnr=0, block_size=block_size, trailer_size=trailer_size)

    fd = os.open(out_file, os.O_WRONLY | os.O_CREAT | os.O_TRUNC)
    for i in range(num_blocks):
        # last = i == num_blocks - 1
        block = next(generator)
        os.write(fd, block)
    os.close(fd)


if __name__ == '__main__':
    main(sys.argv)
