#!/usr/bin/env python

from __future__ import unicode_literals

import os
import subprocess
import unittest

from felix_test_case import FelixTestCase


class TestDatagenStreams16(FelixTestCase):

    script_dir = 'scripts' if os.path.isdir('scripts') else '.'

    def setUp(self):
        try:
            elink = 55
            stream1 = 0x9a
            stream2 = 0xd8
            num_blocks = 1024
            trailer_size = 2
            self.output_file = 'test/datagen/data-55-9a-d8-1024-16.blocks'
            subprocess.check_output(' '.join((os.path.join(self.script_dir, 'felix-block-encode'), str(num_blocks), self.output_file,
                                              '--trailer-size', str(trailer_size),
                                              '--elink', str(elink),
                                              '--stream', str(stream1),
                                              '--stream', str(stream2))),
                                    stderr=subprocess.STDOUT, shell=True)
        except subprocess.CalledProcessError as e:
            print(e.returncode)
            print(e.cmd)
            print(e.output)
            self.assertEqual(e.returncode, 0)

    def test_block_data_55_9a_1024_16(self):
        try:
            subprocess.check_output(' '.join((os.path.join(self.script_dir, 'felix-block-decode'), self.output_file,
                                              '--blocks',
                                              '--nodata',
                                              '--only-error')),
                                    stderr=subprocess.STDOUT, shell=True)
        except subprocess.CalledProcessError as e:
            print(e.returncode)
            print(e.cmd)
            print(e.output)
            self.assertEqual(e.returncode, 0)

    def test_chunks_data_55_9a_1024_16(self):
        try:
            subprocess.check_output(' '.join((os.path.join(self.script_dir, 'felix-block-decode'), self.output_file,
                                              '--chunks',
                                              '--nodata',
                                              '--only-error')),
                                    stderr=subprocess.STDOUT, shell=True)
        except subprocess.CalledProcessError as e:
            print(e.returncode)
            print(e.cmd)
            print(e.output)
            self.assertEqual(e.returncode, 0)

    def test_subchunks_data_55_9a_1024_16(self):
        try:
            subprocess.check_output(' '.join((os.path.join(self.script_dir, 'felix-block-decode'), self.output_file,
                                              '--subchunks',
                                              '--nodata',
                                              '--only-error')),
                                    stderr=subprocess.STDOUT, shell=True)
        except subprocess.CalledProcessError as e:
            print(e.returncode)
            print(e.cmd)
            print(e.output)
            self.assertEqual(e.returncode, 0)


if __name__ == '__main__':
    unittest.main()
