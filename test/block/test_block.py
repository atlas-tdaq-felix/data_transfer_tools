#!/usr/bin/env pytest

import pytest

from felix.block import Block, block_header, Chunk, decode_blocks
from felix.block import decode_block_header, decode_subchunk_trailer
from felix.block import encode_blocks, random_chunks, Subchunk, subchunk_trailer
from felix.block import BLOCK_HEADER_SIZE
from felix.block import _subchunktype, _new_block, _add_chunk_to_block


def test_random_chunks():
    it = random_chunks(8, 8)
    assert bytes(b"\x00\x00\x00\x00\xFF\xFF\xFF\xFF") == next(it)
    assert bytes(b"\x00\x00\x00\x00\xFF\xFF\xFF\xFF") != next(it)
    assert bytes(b"\x02\x00\x02\x00\xFF\xFF\xFF\xFF") == next(it)


def test_random_chunks_stream():
    it = random_chunks(9, 9, 0, [0x9a])
    assert bytes(b"\x9a\x00\x00\x00\x00\xFF\xFF\xFF\xFF") == next(it)
    assert bytes(b"\x9a\x00\x00\x00\x00\xFF\xFF\xFF\xFF") != next(it)
    assert bytes(b"\x9a\x02\x00\x02\x00\xFF\xFF\xFF\xFF") == next(it)


def test_subchunk_trailer_16():
    assert subchunk_trailer('null', 0, 2, False, False, False) == b'\x00\x00'
    assert subchunk_trailer('first', 0, 2, False, False, False) == b'\x00\x20'
    assert subchunk_trailer('last', 0, 2, False, False, False) == b'\x00\x40'
    assert subchunk_trailer('both', 0, 2, False, False, False) == b'\x00\x60'
    assert subchunk_trailer('middle', 0, 2, False, False, False) == b'\x00\x80'

    assert subchunk_trailer('null', 0, 2, True, False, False) == b'\x00\x10'
    assert subchunk_trailer('null', 0, 2, False, True, False) == b'\x00\x08'
    assert subchunk_trailer('null', 0, 2, True, True, False) == b'\x00\x18'
    assert subchunk_trailer('null', 0, 2, False, False, True) == b'\x00\x04'
    assert subchunk_trailer('null', 0, 2, True, False, True) == b'\x00\x14'
    assert subchunk_trailer('null', 0, 2, False, True, True) == b'\x00\x0c'
    assert subchunk_trailer('null', 0, 2, True, True, True) == b'\x00\x1c'

    assert subchunk_trailer('null', 255, 2, False, False, False) == b'\xFF\x00'
    assert subchunk_trailer('null', 256, 2, False, False, False) == b'\x00\x01'
    assert subchunk_trailer('null', 511, 2, False, False, False) == b'\xFF\x01'
    assert subchunk_trailer('null', 1023, 2, False, False, False) == b'\xFF\x03'


def test_subchunk_trailer():
    assert subchunk_trailer('null', 0, 4, False, False, False, False) == b'\x00\x00\x00\x00'
    assert subchunk_trailer('first', 0, 4, False, False, False, False) == b'\x00\x00\x00\x20'
    assert subchunk_trailer('last', 0, 4, False, False, False, False) == b'\x00\x00\x00\x40'
    assert subchunk_trailer('both', 0, 4, False, False, False, False) == b'\x00\x00\x00\x60'
    assert subchunk_trailer('middle', 0, 4, False, False, False, False) == b'\x00\x00\x00\x80'

    assert subchunk_trailer('null', 0, 4, True, False, False, False) == b'\x00\x00\x00\x10'
    assert subchunk_trailer('null', 0, 4, False, True, False, False) == b'\x00\x00\x00\x08'
    assert subchunk_trailer('null', 0, 4, True, True, False, False) == b'\x00\x00\x00\x18'
    assert subchunk_trailer('null', 0, 4, False, False, True, False) == b'\x00\x00\x00\x04'
    assert subchunk_trailer('null', 0, 4, True, False, True, False) == b'\x00\x00\x00\x14'
    assert subchunk_trailer('null', 0, 4, False, True, True, False) == b'\x00\x00\x00\x0c'
    assert subchunk_trailer('null', 0, 4, True, True, True, False) == b'\x00\x00\x00\x1c'
    assert subchunk_trailer('null', 0, 4, False, False, False, True) == b'\x00\x00\x00\x02'
    assert subchunk_trailer('null', 0, 4, True, False, False, True) == b'\x00\x00\x00\x12'
    assert subchunk_trailer('null', 0, 4, False, True, False, True) == b'\x00\x00\x00\x0a'
    assert subchunk_trailer('null', 0, 4, True, True, False, True) == b'\x00\x00\x00\x1a'
    assert subchunk_trailer('null', 0, 4, False, False, True, True) == b'\x00\x00\x00\x06'
    assert subchunk_trailer('null', 0, 4, True, False, True, True) == b'\x00\x00\x00\x16'
    assert subchunk_trailer('null', 0, 4, False, True, True, True) == b'\x00\x00\x00\x0e'
    assert subchunk_trailer('null', 0, 4, True, True, True, True) == b'\x00\x00\x00\x1e'

    assert subchunk_trailer('null', 255, 4, False, False, False, False) == b'\xFF\x00\x00\x00'
    assert subchunk_trailer('null', 256, 4, False, False, False, False) == b'\x00\x01\x00\x00'
    assert subchunk_trailer('null', 511, 4, False, False, False, False) == b'\xFF\x01\x00\x00'
    assert subchunk_trailer('null', 1023, 4, False, False, False, False) == b'\xFF\x03\x00\x00'
    assert subchunk_trailer('null', 2047, 4, False, False, False, False) == b'\xFF\x07\x00\x00'
    assert subchunk_trailer('null', 4095, 4, False, False, False, False) == b'\xFF\x0F\x00\x00'
    assert subchunk_trailer('null', 8191, 4, False, False, False, False) == b'\xFF\x1F\x00\x00'


def test_decode_subchunk_trailer_16():
    def t(sub_type, length, trunc, chunk_error, crc_error):
        trailer = decode_subchunk_trailer(subchunk_trailer(sub_type, length, 2, trunc, chunk_error, crc_error))
        assert trailer['type'] == sub_type
        assert trailer['length'] == length
        assert trailer['trunc'] == trunc
        assert trailer['chunk_error'] == chunk_error
        assert trailer['crc_error'] == crc_error

    t('null', 0, False, False, False)
    t('first', 0, False, False, False)
    t('last', 0, False, False, False)
    t('both', 0, False, False, False)
    t('middle', 0, False, False, False)

    t('null', 0, True, False, False)
    t('null', 0, False, True, False)
    t('null', 0, True, True, False)
    t('null', 0, False, False, True)
    t('null', 0, True, False, True)
    t('null', 0, False, True, True)
    t('null', 0, True, True, True)

    t('null', 255, False, False, False)
    t('null', 2**8, False, False, False)
    t('null', 2**10 - 1, False, False, False)


def test_decode_subchunk_trailer():
    def t(sub_type, length, trunc, chunk_error, crc_error, busy):
        trailer = decode_subchunk_trailer(subchunk_trailer(sub_type, length, 4, trunc, chunk_error, crc_error, busy))
        assert trailer['type'] == sub_type
        assert trailer['length'] == length
        assert trailer['trunc'] == trunc
        assert trailer['chunk_error'] == chunk_error
        assert trailer['crc_error'] == crc_error
        assert trailer['busy'] == busy

    t('null', 0, False, False, False, False)
    t('first', 0, False, False, False, False)
    t('last', 0, False, False, False, False)
    t('both', 0, False, False, False, False)
    t('middle', 0, False, False, False, False)

    t('null', 0, True, False, False, False)
    t('null', 0, False, True, False, False)
    t('null', 0, True, True, False, False)
    t('null', 0, False, False, True, False)
    t('null', 0, True, False, True, False)
    t('null', 0, False, True, True, False)
    t('null', 0, True, True, True, False)
    t('null', 0, False, False, False, True)
    t('null', 0, True, False, False, True)
    t('null', 0, False, True, False, True)
    t('null', 0, True, True, False, True)
    t('null', 0, False, False, True, True)
    t('null', 0, True, False, True, True)
    t('null', 0, False, True, True, True)
    t('null', 0, True, True, True, True)

    t('null', 255, False, False, False, False)
    t('null', 2**8, False, False, False, False)
    t('null', 2**10 - 1, False, False, False, False)
    t('null', 2**11 - 1, False, False, False, False)
    t('null', 2**12 - 1, False, False, False, False)
    t('null', 2**13 - 1, False, False, False, False)


def test_subchunktype():
    # subchunktype, words_left_in_block, words_left_in_chunk
    assert _subchunktype("last", 1000, 10) == 'both'
    assert _subchunktype("middle", 1000, 10) == 'last'
    assert _subchunktype("last", 1, 10) == 'first'
    assert _subchunktype("middle", 1, 10) == 'middle'


def test__add_chunk_to_block_16():
    # add a chunk of size 12 to an empty block of size 16, size 2 should be left, type 'both', next chunk should be size 0
    block = bytearray(16)
    block, blockpos, chunk, subchunk_type = _add_chunk_to_block(block, 0, b"1" * 12, "last", 2)
    assert block == bytes(b"1" * 12 + subchunk_trailer('both', 12, 2)) + b'\x00\x00'
    assert len(block) == 16
    assert blockpos == 14
    assert len(chunk) == 0

    # add following chunk of size 4 of which none fits, except a "null" trailer, filled, type 'both', next chunk should be size 4
    block, blockpos, chunk, subchunk_type = _add_chunk_to_block(block, blockpos, b"2" * 4, "both", 2)
    assert block == bytes(b"1" * 12 + subchunk_trailer('both', 12, 2)) + b'\x00\x00'
    assert len(block) == 16
    assert blockpos == 16
    assert len(chunk) == 4

    # add a chunk of size 32 to an empty block of 16, filled, type 'first', next chunk should be size 18
    block = bytearray(16)
    block, blockpos, chunk, subchunk_type = _add_chunk_to_block(block, 0, b"2" * 32, "both", 2)
    assert block == bytes(b"2" * 14 + subchunk_trailer('first', 14, 2))
    assert len(block) == 16
    assert blockpos == 16
    assert len(chunk) == 32 - 14

    # add a chunk of 18 (previous) to an empty block of 16, filled, type 'middle', next chunk should be size 4
    block = bytearray(16)
    block, blockpos, chunk, subchunk_type = _add_chunk_to_block(block, 0, chunk, "first", 2)
    assert block == bytes(b"2" * 14 + subchunk_trailer('middle', 14, 2))
    assert len(block) == 16
    assert blockpos == 16
    assert len(chunk) == 32 - 14 - 14

    # add a chunk of 4 (previous) to an empty block of 16, size 10 should be left, type 'last', next chunk should be size 0
    block = bytearray(16)
    block, blockpos, chunk, subchunk_type = _add_chunk_to_block(block, 0, chunk, "middle", 2)
    assert block == bytes(b"2" * 4 + subchunk_trailer('last', 4, 2)) + b'\x00' * 10
    assert len(block) == 16
    assert blockpos == 6
    assert len(chunk) == 0

    # add a chunk of size 3 to an empty block of 16, padded by 1 byte, size 10 should be left, type 'both', next chunk should be size 0
    block = bytearray(16)
    block, blockpos, chunk, subchunk_type = _add_chunk_to_block(block, 0, b"333", "last", 2)
    assert block == bytes(b"3" * 3 + b'\x00' + subchunk_trailer('both', 3, 2)) + b'\x00' * 10
    assert len(block) == 16
    assert blockpos == 6
    assert len(chunk) == 0


def test__add_chunk_to_block():
    # add a chunk of size 12 to an empty block of size 20, size 4 should be left, type 'both', next chunk should be size 0
    block = bytearray(20)
    block, blockpos, chunk, subchunk_type = _add_chunk_to_block(block, 0, b"1" * 12, "last", 4)
    assert block == bytes(b"1" * 12 + subchunk_trailer('both', 12, 4)) + b'\x00\x00\x00\x00'
    assert len(block) == 20
    assert blockpos == 16
    assert len(chunk) == 0

    # add following chunk of size 4 of which none fits, except a "null" trailer, filled, type 'both', next chunk should be size 4
    block, blockpos, chunk, subchunk_type = _add_chunk_to_block(block, blockpos, b"2" * 4, "both", 4)
    assert block == bytes(b"1" * 12 + subchunk_trailer('both', 12, 4)) + b'\x00\x00\x00\x00'
    assert len(block) == 20
    assert blockpos == 20
    assert len(chunk) == 4

    # add a chunk of size 36 to an empty block of 20, filled, type 'first', next chunk should be size 20
    block = bytearray(20)
    block, blockpos, chunk, subchunk_type = _add_chunk_to_block(block, 0, b"2" * 36, "both", 4)
    assert block == bytes(b"2" * 16 + subchunk_trailer('first', 16, 4))
    assert len(block) == 20
    assert blockpos == 20
    assert len(chunk) == 36 - 16

    # add a chunk of 20 (previous) to an empty block of 20, filled, type 'middle', next chunk should be size 4
    block = bytearray(20)
    block, blockpos, chunk, subchunk_type = _add_chunk_to_block(block, 0, chunk, "first", 4)
    assert block == bytes(b"2" * 16 + subchunk_trailer('middle', 16, 4))
    assert len(block) == 20
    assert blockpos == 20
    assert len(chunk) == 36 - 16 - 16

    # add a chunk of 4 (previous) to an empty block of 20, size 12 should be left, type 'last', next chunk should be size 0
    block = bytearray(20)
    block, blockpos, chunk, subchunk_type = _add_chunk_to_block(block, 0, chunk, "middle", 4)
    assert block == bytes(b"2" * 4 + subchunk_trailer('last', 4, 4)) + b'\x00' * 12
    assert len(block) == 20
    assert blockpos == 8
    assert len(chunk) == 0

    # add a chunk of size 3 to an empty block of 20, padded by 1 byte, size 12 should be left, type 'both', next chunk should be size 0
    block = bytearray(20)
    block, blockpos, chunk, subchunk_type = _add_chunk_to_block(block, 0, b"333", "last", 4)
    assert block == bytes(b"3" * 3 + b'\x00' + subchunk_trailer('both', 3, 4)) + b'\x00' * 12
    assert len(block) == 20
    assert blockpos == 8
    assert len(chunk) == 0

    # add a chunk of size 2 to an empty block of 20, padded by 2 bytes, size 12 should be left, type 'both', next chunk should be size 0
    block = bytearray(20)
    block, blockpos, chunk, subchunk_type = _add_chunk_to_block(block, 0, b"33", "last", 4)
    assert block == bytes(b"3" * 2 + b'\x00' * 2 + subchunk_trailer('both', 2, 4)) + b'\x00' * 12
    assert len(block) == 20
    assert blockpos == 8
    assert len(chunk) == 0

    # add a chunk of size 1 to an empty block of 20, padded by 3 bytes, size 12 should be left, type 'both', next chunk should be size 0
    block = bytearray(20)
    block, blockpos, chunk, subchunk_type = _add_chunk_to_block(block, 0, b"3", "last", 4)
    assert block == bytes(b"3" * 1 + b'\x00' * 3 + subchunk_trailer('both', 1, 4)) + b'\x00' * 12
    assert len(block) == 20
    assert blockpos == 8
    assert len(chunk) == 0


def test_block_header():
    block_size = 1024
    assert block_header(0xABCD, 0, 0, block_size) == b'\x00\x00\xCD\xAB'
    assert block_header(0xABCD, 0, 1, block_size) == b'\x00\x08\xCD\xAB'
    assert block_header(0xABCD, 0, 31, block_size) == b'\x00\xF8\xCD\xAB'
    assert block_header(0xABCD, 1, 0, block_size) == b'\x01\x00\xCD\xAB'
    assert block_header(0xABCD, 2047, 0, block_size) == b'\xFF\x07\xCD\xAB'
    assert block_header(0xABCD, 2047, 31, block_size) == b'\xFF\xFF\xCD\xAB'
    assert block_header(0xACCC, 0, 0, block_size) == b'\x00\x00\xCC\xAC'


def test_decode_block_header():
    def h(elink, seqnr):
        block_size = 1024
        header = decode_block_header(block_header(0xABCD, elink, seqnr, block_size))
        assert header['sob'] == 0xABCD
        assert header['elink'] == elink
        assert header['seqnr'] == seqnr
        assert header['block_size'] == block_size

    h(0, 0)
    h(0, 1)
    h(0, 31)
    h(2047, 0)
    h(2047, 31)


def test_new_block():
    block, blockpos, seqnr = _new_block(0xABCD, 0, 0, 1024)
    assert blockpos == 4
    assert seqnr == 1
    assert block[2:4] == b'\xCD\xAB'
    assert len(block) == 1024

    block, blockpos, seqnr = _new_block(0xABCD, 0, 31, 1024)
    assert blockpos == 4
    assert seqnr == 0
    assert block[2:4] == b'\xCD\xAB'


def test_encode_blocks_16():
    trailer_size = 2
    block_size = 1024
    pad_size = block_size - BLOCK_HEADER_SIZE - 4 - trailer_size - 4 - trailer_size
    fill_size = block_size - BLOCK_HEADER_SIZE - trailer_size
    chunks = iter([b'1111', b'2222'])
    block = next(encode_blocks(chunks, 0xABCD, 0, block_size, 0, trailer_size))
    correct_block = block_header(0xABCD, 0, 0, block_size) + \
        b'1111' + subchunk_trailer('both', 4, trailer_size) + \
        b'2222' + subchunk_trailer('both', 4, trailer_size) + b'\x00' * pad_size
    assert len(block) == block_size
    assert len(correct_block) == block_size
    assert block == correct_block

    chunk = block_size * 2 * b'1'
    block = next(encode_blocks(iter([chunk]), 0xABCD, 0, block_size, 0, trailer_size))
    assert block == block_header(0xABCD, 0, 0, block_size) + b'1' * fill_size + subchunk_trailer('first', fill_size, trailer_size)


def test_encode_blocks_1024():
    trailer_size = 4
    block_size = 1024
    pad_size = block_size - BLOCK_HEADER_SIZE - 4 - trailer_size - 4 - trailer_size
    fill_size = block_size - BLOCK_HEADER_SIZE - trailer_size
    chunks = iter([b'1111', b'2222'])
    block = next(encode_blocks(chunks, 0xC0CE, 0, block_size))
    correct_block = block_header(0xC0CE, 0, 0, block_size) + \
        b'1111' + subchunk_trailer('both', 4, trailer_size) + \
        b'2222' + subchunk_trailer('both', 4, trailer_size) + b'\x00' * pad_size
    assert len(block) == block_size
    assert len(correct_block) == block_size
    assert block == correct_block

    chunk = block_size * 2 * b'1'
    block = next(encode_blocks(iter([chunk]), 0xC0CE, 0, block_size))
    assert block == block_header(0xC0CE, 0, 0, block_size) + b'1' * fill_size + subchunk_trailer('first', fill_size, trailer_size)


def test_encode_blocks_4096():
    trailer_size = 4
    block_size = 4096
    pad_size = block_size - BLOCK_HEADER_SIZE - 4 - trailer_size - 4 - trailer_size
    fill_size = block_size - BLOCK_HEADER_SIZE - trailer_size
    chunks = iter([b'1111', b'2222'])
    block = next(encode_blocks(chunks, 0xC0CE, 0, block_size))
    correct_block = block_header(0xC0CE, 0, 0, block_size) + \
        b'1111' + subchunk_trailer('both', 4, trailer_size) + \
        b'2222' + subchunk_trailer('both', 4, trailer_size) + b'\x00' * pad_size
    assert len(block) == block_size
    assert len(correct_block) == block_size
    assert block == correct_block

    chunk = block_size * 2 * b'1'
    block = next(encode_blocks(iter([chunk]), 0xC0CE, 0, block_size))
    assert block == block_header(0xC0CE, 0, 0, block_size) + b'1' * fill_size + subchunk_trailer('first', fill_size, trailer_size)


@pytest.fixture
def single_chunk_block_16():
    sob = 0xABCD
    block_size = 1024
    trailer_size = 2
    block, _, _ = _new_block(sob, 0, 0, block_size)
    block[block_size - trailer_size:block_size] = subchunk_trailer('both', (block_size - BLOCK_HEADER_SIZE - trailer_size), trailer_size)
    return Block.from_bytes(block)


def test_block_from_bytes_16(single_chunk_block_16):
    block_size = 1024
    trailer_size = 2
    fill_size = block_size - BLOCK_HEADER_SIZE - trailer_size
    assert len(single_chunk_block_16.data) == block_size - BLOCK_HEADER_SIZE
    assert single_chunk_block_16.elink == 0
    assert single_chunk_block_16.seqnr == 0
    assert single_chunk_block_16.block_size == block_size
    assert single_chunk_block_16.data == b'\x00' * (block_size - BLOCK_HEADER_SIZE - trailer_size) + subchunk_trailer('both', fill_size, trailer_size)
    assert single_chunk_block_16.data[-trailer_size:] == subchunk_trailer('both', fill_size, trailer_size)
    assert decode_subchunk_trailer(single_chunk_block_16.data[-trailer_size:])['length'] == fill_size


def test_block_subchunks_16(single_chunk_block_16):
    block_size = 1024
    trailer_size = 2

    count = 0
    for sc in single_chunk_block_16.subchunks():
        count += 1
        last_sc = sc

    assert count == 1
    assert len(last_sc) == block_size - BLOCK_HEADER_SIZE - trailer_size
    assert last_sc.block == single_chunk_block_16
    assert len(last_sc.data) == block_size - BLOCK_HEADER_SIZE - trailer_size


@pytest.fixture
def single_chunk_block_1024():
    sob = 0xC0CE
    block_size = 1024
    trailer_size = 4
    block, _, _ = _new_block(sob, 0, 0, block_size)
    block[block_size - trailer_size:block_size] = subchunk_trailer('both', (block_size - BLOCK_HEADER_SIZE - trailer_size), trailer_size)
    return Block.from_bytes(block)


def test_block_from_bytes_1024(single_chunk_block_1024):
    block_size = 1024
    trailer_size = 4
    fill_size = block_size - BLOCK_HEADER_SIZE - trailer_size
    assert len(single_chunk_block_1024.data) == block_size - BLOCK_HEADER_SIZE
    assert single_chunk_block_1024.elink == 0
    assert single_chunk_block_1024.seqnr == 0
    assert single_chunk_block_1024.block_size == block_size
    assert single_chunk_block_1024.data == b'\x00' * (block_size - BLOCK_HEADER_SIZE - trailer_size) \
        + subchunk_trailer('both', fill_size, trailer_size)
    assert single_chunk_block_1024.data[-trailer_size:] == subchunk_trailer('both', fill_size, trailer_size)
    assert decode_subchunk_trailer(single_chunk_block_1024.data[-trailer_size:])['length'] == fill_size


def test_block_subchunks_1024(single_chunk_block_1024):
    block_size = 1024
    trailer_size = 4

    count = 0
    for sc in single_chunk_block_1024.subchunks():
        count += 1
        last_sc = sc

    assert count == 1
    assert len(last_sc) == block_size - BLOCK_HEADER_SIZE - trailer_size
    assert last_sc.block == single_chunk_block_1024
    assert len(last_sc.data) == block_size - BLOCK_HEADER_SIZE - trailer_size


@pytest.fixture
def single_chunk_block_4096():
    sob = 0xC0CE
    block_size = 4096
    trailer_size = 4
    block, _, _ = _new_block(sob, 0, 0, block_size)
    block[block_size - trailer_size:block_size] = subchunk_trailer('both', (block_size - BLOCK_HEADER_SIZE - trailer_size), trailer_size)
    return Block.from_bytes(block)


def test_block_from_bytes_4096(single_chunk_block_4096):
    block_size = 4096
    trailer_size = 4
    fill_size = block_size - BLOCK_HEADER_SIZE - trailer_size
    assert len(single_chunk_block_4096.data) == block_size - BLOCK_HEADER_SIZE
    assert single_chunk_block_4096.elink == 0
    assert single_chunk_block_4096.seqnr == 0
    assert single_chunk_block_4096.block_size == block_size
    assert single_chunk_block_4096.data == b'\x00' * (block_size - BLOCK_HEADER_SIZE - trailer_size) \
        + subchunk_trailer('both', fill_size, trailer_size)
    assert single_chunk_block_4096.data[-trailer_size:] == subchunk_trailer('both', fill_size, trailer_size)
    assert decode_subchunk_trailer(single_chunk_block_4096.data[-trailer_size:])['length'] == fill_size


def test_block_subchunks_4096(single_chunk_block_4096):
    block_size = 4096
    trailer_size = 4

    count = 0
    for sc in single_chunk_block_4096.subchunks():
        count += 1
        last_sc = sc

    assert count == 1
    assert len(last_sc) == block_size - BLOCK_HEADER_SIZE - trailer_size
    assert last_sc.block == single_chunk_block_4096
    assert len(last_sc.data) == block_size - BLOCK_HEADER_SIZE - trailer_size


@pytest.fixture
def subchunk_16(single_chunk_block_16):
    block_size = 1024
    trailer_size = 2
    subchunk = bytearray(block_size - BLOCK_HEADER_SIZE)
    subchunk[-trailer_size:] = subchunk_trailer('both', block_size - BLOCK_HEADER_SIZE - trailer_size, trailer_size)
    return Subchunk.from_bytes(single_chunk_block_16, subchunk)


def test_subchunk_from_bytes_16(subchunk_16):
    block_size = 1024
    trailer_size = 2
    assert len(subchunk_16) == block_size - BLOCK_HEADER_SIZE - trailer_size


def test_chunk_16(subchunk_16):
    block_size = 1024
    trailer_size = 2
    chunk = Chunk()
    chunk.add_subchunk(subchunk_16)
    assert chunk.state == 'full'
    assert len(chunk) == block_size - BLOCK_HEADER_SIZE - trailer_size
    assert subchunk_16.block in chunk.blocks


@pytest.fixture
def subchunk_1024(single_chunk_block_1024):
    block_size = 1024
    trailer_size = 4
    subchunk = bytearray(block_size - BLOCK_HEADER_SIZE)
    subchunk[-trailer_size:] = subchunk_trailer('both', block_size - BLOCK_HEADER_SIZE - trailer_size, trailer_size)
    return Subchunk.from_bytes(single_chunk_block_1024, subchunk)


def test_subchunk_from_bytes_1024(subchunk_1024):
    block_size = 1024
    trailer_size = 4
    assert len(subchunk_1024) == block_size - BLOCK_HEADER_SIZE - trailer_size


def test_chunk_1024(subchunk_1024):
    block_size = 1024
    trailer_size = 4
    chunk = Chunk()
    chunk.add_subchunk(subchunk_1024)
    assert chunk.state == 'full'
    assert len(chunk) == block_size - BLOCK_HEADER_SIZE - trailer_size
    assert subchunk_1024.block in chunk.blocks


@pytest.fixture
def subchunk_4096(single_chunk_block_4096):
    block_size = 4096
    trailer_size = 4
    subchunk = bytearray(block_size - BLOCK_HEADER_SIZE)
    subchunk[-trailer_size:] = subchunk_trailer('both', block_size - BLOCK_HEADER_SIZE - trailer_size, trailer_size)
    return Subchunk.from_bytes(single_chunk_block_4096, subchunk)


def test_subchunk_from_bytes_4096(subchunk_4096):
    block_size = 4096
    trailer_size = 4
    assert len(subchunk_4096) == block_size - BLOCK_HEADER_SIZE - trailer_size


def test_chunk_4096(subchunk_4096):
    block_size = 4096
    trailer_size = 4
    chunk = Chunk()
    chunk.add_subchunk(subchunk_4096)
    assert chunk.state == 'full'
    assert len(chunk) == block_size - BLOCK_HEADER_SIZE - trailer_size
    assert subchunk_4096.block in chunk.blocks


def test_decode_blocks_simple_16():
    sob = 0xABCD
    block_size = 1024
    trailer_size = 2
    chunks = [b'0' * 128, b'1' * 128, b'2' * 128, b'3' * 128]

    decoded_chunks = decode_blocks(encode_blocks(chunks, sob, 0, block_size, 0, trailer_size))

    for d, c in zip(decoded_chunks, chunks):
        assert d.data == c
        print(d.data, c)


def test_decode_blocks_16():
    sob = 0xABCD
    block_size = 1024
    trailer_size = 2
    chunks = [b'0' * 128, b'1' * 334, b'2' * 4096, b'3' * 16]

    decoded_chunks = decode_blocks(encode_blocks(chunks, sob, 0, block_size, 0, trailer_size))

    for d, c in zip(decoded_chunks, chunks):
        print(d.data, c)
        assert d.data == c


def test_encode_blocks_full_block_16():
    sob = 0xABCD
    block_size = 1024
    trailer_size = 2
    chunks = [b'0' * 1016, b'1' * 5134]

    blocks = list(encode_blocks(chunks, sob, 0, block_size, 0, trailer_size))
    assert len(blocks) == 7


def test_decode_blocks_full_block_16():
    sob = 0xABCD
    block_size = 1024
    trailer_size = 2
    chunks = [b'0' * 1016, b'1' * 5134]

    decoded_chunks = decode_blocks(encode_blocks(chunks, sob, 0, block_size, 0, trailer_size), yield_error_chunks=True)

    for d, c in zip(decoded_chunks, chunks):
        print(d.data, c)
        assert d.data == c
        assert d.state != 'error'


def test_decode_blocks_simple_1024():
    sob = 0xC0CE
    block_size = 1024
    trailer_size = 4
    chunks = [b'0' * 128, b'1' * 128, b'2' * 128, b'3' * 128]

    decoded_chunks = decode_blocks(encode_blocks(chunks, sob, 0, block_size, 0, trailer_size))

    for d, c in zip(decoded_chunks, chunks):
        assert d.data == c
        print(d.data, c)


def test_decode_blocks_1024():
    sob = 0xC0CE
    block_size = 1024
    trailer_size = 4
    chunks = [b'0' * 128, b'1' * 334, b'2' * 4096, b'3' * 16]

    decoded_chunks = decode_blocks(encode_blocks(chunks, sob, 0, block_size, 0, trailer_size))

    for d, c in zip(decoded_chunks, chunks):
        print(d.data, c)
        assert d.data == c


def test_encode_blocks_full_block_1024():
    sob = 0xC0CE
    block_size = 1024
    trailer_size = 4
    chunks = [b'0' * 1016, b'1' * 5134]

    blocks = list(encode_blocks(chunks, sob, 0, block_size, 0, trailer_size))
    assert len(blocks) == 7


def test_decode_blocks_full_block_1024():
    sob = 0xC0CE
    block_size = 1024
    trailer_size = 4
    chunks = [b'0' * 1016, b'1' * 5134]

    decoded_chunks = decode_blocks(encode_blocks(chunks, sob, 0, block_size, 0, trailer_size), yield_error_chunks=True)

    for d, c in zip(decoded_chunks, chunks):
        print(d.data, c)
        assert d.data == c
        assert d.state != 'error'


def test_decode_blocks_simple_4096():
    sob = 0xC0CE
    block_size = 4096
    trailer_size = 4
    chunks = [b'0' * 128, b'1' * 128, b'2' * 128, b'3' * 128]

    decoded_chunks = decode_blocks(encode_blocks(chunks, sob, 0, block_size, 0, trailer_size))

    for d, c in zip(decoded_chunks, chunks):
        print(d.data, c)
        assert d.data == c


def test_decode_blocks_4096():
    sob = 0xC0CE
    block_size = 4096
    trailer_size = 4
    chunks = [b'0' * 128, b'1' * 334, b'2' * 4096, b'3' * 16]

    decoded_chunks = decode_blocks(encode_blocks(chunks, sob, 0, block_size, 0, trailer_size))

    for d, c in zip(decoded_chunks, chunks):
        print(d.data, c)
        assert d.data == c


def test_encode_blocks_full_block_4096():
    sob = 0xC0CE
    block_size = 4096
    trailer_size = 4
    chunks = [b'0' * 1016, b'1' * 5134]

    blocks = list(encode_blocks(chunks, sob, 0, block_size, 0, trailer_size))
    assert len(blocks) == 2


def test_decode_blocks_full_block_4096():
    sob = 0xC0CE
    block_size = 4096
    trailer_size = 4
    chunks = [b'0' * 1016, b'1' * 5134]

    decoded_chunks = decode_blocks(encode_blocks(chunks, sob, 0, block_size, 0, trailer_size), yield_error_chunks=True)

    for d, c in zip(decoded_chunks, chunks):
        print(d.data, c)
        assert d.data == c
        assert d.state != 'error'
