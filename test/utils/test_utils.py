#!/usr/bin/env pytest

import os

import pytest

from felix.utils import datagen


@pytest.fixture
def fds(request):
    names = ['__testa', '__testb']
    for fifo in names:
        open(fifo, 'w').close()

    def fin():
        for fifo in names:
            os.unlink(fifo)
    request.addfinalizer(fin)

    return [os.open(name, os.O_WRONLY) for name in names]


def test_datagen(fds):

    class PacketGenerator:
        def __init__(self):
            self.calls = 0

        def packet_generator(self):
            while True:
                self.calls += 1
                yield b'\x00' * 1024

    pg = PacketGenerator()

    data_written, packets_sent, time = datagen(fds, dict((fd, pg.packet_generator()) for fd in fds), num_packets=3)
    print(data_written)
    print(packets_sent)
    assert pg.calls == len(fds) * 3
    for fd in fds:
        assert packets_sent[fd] == 3
        assert data_written[fd] == 3 * 1024
