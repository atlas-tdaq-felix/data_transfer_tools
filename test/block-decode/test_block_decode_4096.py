#!/usr/bin/env python3

import os
import subprocess
import unittest

from felix_test_case import FelixTestCase


class TestBlockDecode4096(FelixTestCase):

    script_dir = 'scripts' if os.path.isdir('scripts') else '.'

    def setUp(self):
        self.output_file = 'test/block-decode/data-55-1024-4096.blocks'

    def test_block_data_55_1024_4096(self):
        try:
            output = subprocess.check_output(' '.join((os.path.join(self.script_dir, 'felix-block-decode'), self.output_file,
                                                       '--blocks',
                                                       '--nodata',
                                                       '--only-error',
                                                       '--count')),
                                             stderr=subprocess.STDOUT, shell=True, encoding='UTF-8')
            self.assertEqual('1024', output)
        except subprocess.CalledProcessError as e:
            print(e.returncode)
            print(e.cmd)
            print(e.output)
            self.assertEqual(e.returncode, 0)

    def test_chunks_data_55_1024_4096(self):
        try:
            output = subprocess.check_output(' '.join((os.path.join(self.script_dir, 'felix-block-decode'), self.output_file,
                                                       '--chunks',
                                                       '--nodata',
                                                       '--only-error',
                                                       '--count')),
                                             stderr=subprocess.STDOUT, shell=True, encoding='UTF-8')
            self.assertEqual('1957', output)
        except subprocess.CalledProcessError as e:
            print(e.returncode)
            print(e.cmd)
            print(e.output)
            self.assertEqual(e.returncode, 0)

    def test_subchunks_data_55_1024_4096(self):
        try:
            output = subprocess.check_output(' '.join((os.path.join(self.script_dir, 'felix-block-decode'), self.output_file,
                                                       '--subchunks',
                                                       '--nodata',
                                                       '--only-error',
                                                       '--count')),
                                             stderr=subprocess.STDOUT, shell=True, encoding='UTF-8')
            self.assertEqual('2977', output)
        except subprocess.CalledProcessError as e:
            print(e.returncode)
            print(e.cmd)
            print(e.output)
            self.assertEqual(e.returncode, 0)


if __name__ == '__main__':
    unittest.main()
