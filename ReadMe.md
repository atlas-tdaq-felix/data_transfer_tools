NOTE: currently out of date with python3, 
tools to be used are felix-block-encode, merge-block-files, felix-block-decode

# FELIX Data Transfer Tools

## Dependencies

- Python 2.6
- docopt 

## Installation in a Virtual Environment

    $ virtualenv --python=python2.6 venv
    $ source venv/bin/activate
    $ ./setup.py install


## Setup of a Development Environment

1) Create a new virtual environment:

        $ virtualenv venv
        $ source venv/bin/activate

3) Install necessary dependencies:

        $ pip install -r requirements.txt

3) Install the package in development mode:

        $ python setup.py develop


## Program Usage

    $ felix-datagen --help
    $ felix-block-decode --help

## Examples

1) Generate data on UNIX FIFOs (emulate a GBT interface device)

        $ felix-datagen FIFO_0 FIFO_1 FIFO_2 ...

    If no FIFOs are given, by default 960 FIFOs are created.


2) Generate random transfer data and analyze blocks or chunks

        $ felix-datagen myfifo && cat < myfifo | felix-block-decode --chunks | more
        $ felix-datagen myfifo && cat < myfifo | felix-block-decode --blocks | more

3) Generate random transfer data and save it to a text file

        $ felix-datagen myfifo && cat < myfifo > felix_transfer_data.bin

4) Analyze saved transfer data

        $ felix-block-decode --chunks felix_transfer_data.bin
        $ felix-block-decode --blocks felix_transfer_data.bin

